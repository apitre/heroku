module.exports = function(app) {

	app.get('/snake', function (req, res) {
		res.render('/snake/index.html', {});
	});

	app.get('/ball', function (req, res) {
		res.render('/ball/index.html', {});
	});

	app.get('/sodoku', function (req, res) {
		res.render('/sodoku/index.html', {});
	});

	
}
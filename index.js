var express 	= require('express');
var app 		= express();
var path 		= require('path');
var fs 			= require('fs');
var bodyParser  = require('body-parser');

var port = process.env.PORT || 3000;

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.set('views', __dirname + '/public');

app.set('view engine', 'ejs');

require("./routes.js")(app);

app.listen(port, function () {
  console.log('Example app listening on port '+port+'!');
});
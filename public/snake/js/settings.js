function Settings() {

	var private = {};
	var public = {};

	private.size = 16;

	private.data = {};

	private.init = function() {
		private.data = {
			'score'		: 0,
			'size' 		: private.size,
			'width' 	: private.getX(),
			'height'	: private.getY(),
			'layers'	: 10
		};
	};

	private.getX = function() {
		var width = $("#board").width() - 20;
		return (width - (width % private.size)) / private.size
	};

	private.getY = function() {
		var height = $("#board").height() - 20;
		return (height - (height % private.size)) / private.size
	};

	public.incrementScore = function() {
		private.data.score += 10;
	};

	public.getData = function() {
		return private.data;
	};

	private.init();
	return public;
	
}

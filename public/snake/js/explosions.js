function ExplosionCanvas(canvas) {
    var public = {};
    var private = {};

    private.canvas  = canvas
    private.ctx     = canvas.getContext('2d');
    private.dots    = [];
    private.timer   = null;

    private.removeDots = function() {
        for(var l = private.dots.length - 1, i = l; i >= 0; i--) {
            if(private.dots[i].life < 0) {
                private.dots[i] = private.dots[private.dots.length - 1];
                private.dots.length--;
            }
        }
    };

    private.clear = function() {
        private.ctx.clearRect(0, 0, private.canvas.width, private.canvas.height);
    };

    private.draw = function() {
        private.ctx.fillStyle = "#000";
    
        private.removeDots();
    
        for(var i = 0; i < private.dots.length; i++) {
            var dot = private.dots[i];
            private.ctx.fillRect(dot.x - 1, dot.y - 1, 2, 2);
            dot.update();
        }
    };

    public.boom = function(coordX, coordY) {
    
        var dotAmt = Math.random() * 20 + 50;
    
        for(var i = 0; i < dotAmt; i++) {
            
            var dir     = Math.random() * 2 * Math.PI;
            var speed   = Math.random() * 3 + 2;
            var life    = Math.random() * 10 + 10;
            
            private.dots[private.dots.length] = private.createDot(coordX, coordY, speed, dir, life);
            
        }
    
    };

    private.update = function() {
        if(private.timer != null) {
            clearTimeout(private.timer);
        }

        private.clear();
        private.draw();
    
        private.timer = setTimeout(private.update, 33);
    };  

    private.createDot = function(x, y, speed, dir, life) {
        var xInc = Math.cos(dir) * speed;
        var yInc = Math.sin(dir) * speed;

        var dot = {
            'x'     : x,
            'y'     : y,
            'life'  : life
        };

        dot.update = function() {
            dot.x += xInc;
            dot.y += yInc;
            dot.life--;
        }

        return dot;
    };

    private.update();
    return public;
}

function Map(settings) {

	var private = {};
	var public = {};

	private.specs = settings.getData();
	private.positions = [];

	private.init = function() {
		private.loadMap();
	};

	private.loadMap = function() {
		for (var x = 0; x < private.specs.width; x++) {
			for (var y = 0; y < private.specs.height; y++) {
				private.analysePattern(x, y);
			}
		};
	};

	public.getRandomBlockPosition = function() {
		var randomIndex = (Math.floor(Math.random() * private.positions.length));
		return private.positions[randomIndex];
	};

	public.getPositions = function() {
		return private.positions;
	};

	private.analysePattern = function(x, y) {
		/*
		if (x > 10 && x < private.specs.height && (y > 12 || y < 10)) {
			return false;
		}
		*/

		private.positions.push({ 'x' : x, 'y' : y });
	}

	private.init();
	return public;

}
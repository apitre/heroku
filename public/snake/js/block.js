function Block(x, y) {
	var private = {};
	var public = {};

	public.x = x;
	public.y = y;
	public.isActive = false;

	private.ctx = undefined;
	private.size = undefined;
	private.color = "#ffffff";

	public.setCtx = function(ctx) {
		private.ctx = ctx;
	};

	public.draw = function() {
		var position = private.getPosition();
		private.ctx.fillStyle = private.color;
		private.ctx.fillRect(position.x, position.y, private.size, private.size);
	};

	private.getPosition = function() {
		return {
			'x' : public.x * private.size,
			'y' : public.y * private.size
		}
	};

	public.setSize = function(size) {
		private.size = size;
	};

	public.activate = function() {
		public.isActive = true;
		private.color = "#000000";
		public.draw();
	};

	public.desactivate = function() {
		public.isActive = false;
		private.color = "#ffffff";
		public.draw();
	};

	return public;
}
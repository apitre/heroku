function Board(settings) {
	var private = {};
	var public = {};

	private.specs 		= settings.getData();
	private.ctx	        = undefined;
	private.grid 		= {};
	private.blocks 		= [];
	private.target 		= undefined;
	private.map 		= undefined;

	public.getTarget = function() {
		return private.target;
	};

	public.setMap = function(map) {
		private.map = map;
	};

	public.setCtx = function(ctx) {
		private.ctx = ctx;
	};

	public.generateRandomTarget = function() {
		if (private.target != undefined) {
			private.target.desactivate();
		}

		private.target = public.getRandomBlock();

		if (private.target.isActive) {
			public.generateRandomTarget();
		} else {
			private.target.activate();
		}
	};

	public.getRandomBlock = function() {
		var position = private.map.getRandomBlockPosition();
		return public.getBlock(position.x, position.y);
	};

	public.getBlock = function(x, y) {
		if (private.grid[x] == undefined || private.grid[x][y] == undefined) {
			return undefined;
		}
		return private.grid[x][y];
	};

	public.build = function() {
  		var positions = private.map.getPositions();

		for (var i = 0; i < positions.length; i++) {

			var position = positions[i];

			if (private.grid[position.x] == undefined) {
				private.grid[position.x] = {};
			}

			private.grid[position.x][position.y] = private.createBlock(position);

		};
	};

	private.createBlock = function(position) {
		var block = new Block(position.x, position.y);

		block.setCtx(private.ctx);

		block.setSize(private.specs.size);

		block.draw();

		return block;
	};

	return public;
}
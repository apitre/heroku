function Canvas(settings) {
	var private = {};
	var public = {};

	private.specs 	= settings.getData();
	private.canvas 	= [];

	var $layers = undefined;

	private.init = function() {
		$layers = $('<div id="layers">');

		$layers.css({
			'height' : private.getHeight() + "px", 
			'width'	 : private.getWidth() + "px"
		});

		private.initLayers();
	};

	private.initLayers = function() {
		for (var i = 0; i < private.specs.layers; i++) {
			private.buildLayer(i);
		};
	};

	private.getWidth = function() {
		return private.specs.size * private.specs.width;
	};

	private.getHeight = function() {
		return private.specs.size * private.specs.height;
	};

	private.buildLayer = function(index) {
		var canvas = document.createElement('canvas');

		canvas.id     			= "layer-"+index;
		canvas.width  			= private.getWidth();
		canvas.height 			= private.getHeight();
		canvas.style.zIndex   	= index;

		private.canvas.push(canvas);

		$layers.append($(canvas));
	};

	public.getCanvas = function(index) {
		return private.canvas[index];
	};

	public.getHtml = function() {
		return $layers;
	};

	public.getCtx = function(index) {
		return private.canvas[index].getContext('2d');
	};

	private.init();
	return public;
}
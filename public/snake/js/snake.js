function Snake() {

	var private = {};
	var public = {};

	private.board = undefined;
	private.blocks = [];

	public.add = function(block) {
		private.blocks.push(block);
		block.activate();
	};

	public.setBoard = function(board) {
		private.board = board;
	};

	public.clear = function() {
		for (var i = 0; i < private.blocks.length; i++) {
			private.blocks[i].desactivate();
		}
		private.blocks = [];
	};

	public.setFirst = function(block, isEat) {
		var lastBlock = private.getLast();

		lastBlock.desactivate();

		if (!isEat) {
			private.blocks.pop();
		}

		block.activate();

		var tempArray = [];

		tempArray[0] = block;

		private.blocks = tempArray.concat(private.blocks);;
	};

	private.getLast = function() {
		return private.blocks[private.blocks.length - 1];
	};

	public.getHead = function() {
		return private.blocks[0];
	};

	return public;

}

function Toolbar() {

	var private = {};
	var public = {};

	private.statistic = undefined;

	var $toolbar = undefined;
	var $score = undefined;

	private.init = function() {
		$toolbar = $('<div class="toolbar"></div>');
		$score = $('<div class="score"></div>');
		$toolbar.append($score);
	};

	public.getElement = function() {
		return $toolbar;
	};

	public.update = function() {
		private.render();
	};

	private.render = function() {
		var data = private.statistic.getData();
		$score.html(data.score);
	};

	public.setStatistic = function(statistic) {
		private.statistic = statistic;
	};

	private.init();
	return public;
	
}

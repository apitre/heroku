function Direction() {

	var private = {};
	var public = {};

	public.getNext 	= undefined;

	public.mapKeycode = function(keycode) {
		switch (keycode) {
			case 32 :
				public.getNext = private.down;
				$(window).trigger("startGame");
				break;
			case 37 :
				if (public.getNext != private.right) {
					public.getNext = private.left;
				}
				break;
			case 38 :
				if (public.getNext != private.down) {
					public.getNext = private.up;
				}
				break;
			case 39 :
				if (public.getNext != private.left) {
					public.getNext = private.right;
				}
				break;
			case 40 :
				if (public.getNext != private.up) {
					public.getNext = private.down;
				}
				break;
		}
	}

	private.up = function() {
		return { x : 0, y : -1};
	};

	private.down = function() {
		return { x : 0, y : 1};
	};

	private.left = function() {
		return { x : -1, y : 0};
	};

	private.right = function() {
		return { x : 1, y : 0};
	};

	return public;
}

function Game() {

	var private = {};
	var public = {};

	var $game 		= $('body');
	var $board 		= $("#board");
	var $keyboard 	= $("#keyboard");

	private.map 		= undefined;
	private.board 		= undefined;
	private.loop 		= undefined;
	private.snake 		= undefined;
	private.direction 	= undefined;
	private.toolbar 	= undefined;
	private.settings 	= undefined;
	private.explosions	= undefined;

	public.canvas 		= undefined;

	private.main = function() {

		private.settings = new Settings();	

		private.direction = new Direction();

		private.initKeyboard();

		private.loop = new Loop();

		private.initCanvas();

		private.initExplosionCtx();

		private.initLoop();

		private.initBoard();

		private.initSnake();

		//private.initToolbar();

		$(window).on('hitError', private.hitError);

		$(window).on('finishLoading', private.board.clearLoader);

		$(window).on('startGame', private.startGame);

	};

	private.initLoop = function() {
		private.loop = new Loop();
		private.loop.on('render', private.render);
	};

	private.initExplosionCtx = function() {
		var canvas = public.canvas.getCanvas(1);
		private.explosions = new ExplosionCanvas(canvas);
	}

	private.initCanvas = function() {
		public.canvas = new Canvas(private.settings);
		var $canvas = public.canvas.getHtml();
		$board.append($canvas);
	};

	private.initKeyboard = function() {
		var keyboard = new Keyboard(private.direction);

		var virtualKeyboard = new VirtualKeyboard(private.direction);

		$keyboard.append(virtualKeyboard.getHtml());
	};

	private.startGame = function() {
		if (private.loop.isInactive()) {
			private.loop.run();
		}
	};

	private.initToolbar = function() {
		private.toolbar = new Toolbar();
		private.toolbar.setStatistic(private.settings);
		private.toolbar.update();

		$game.append(private.toolbar.getElement());
	};

	private.initBoard = function() {
		var map 	= new Map(private.settings);
		var ctx 	= public.canvas.getCtx(0);

		private.board = new Board(private.settings);

		private.board.setCtx(ctx);
		private.board.setMap(map);
		private.board.build();
		private.board.generateRandomTarget();
	};

	private.hitError = function() {
		$game.addClass("error");
		private.loop.stop();
	};

	public.reset = function() {
		private.snake.clear();
	};

	private.initSnake = function() {
		private.snake = new Snake();

		for (var i = 0; i <= 4; i++) {
			var block = private.board.getBlock(5, (10 - i));
			private.snake.add(block);
		};
	};

	private.setLoop = function() {
		private.loop = new Loop();
		private.loop.on('render', private.render);
		private.loop.run();
	};

	private.getNext = function() {
		var next = private.direction.getNext();
		var head = private.snake.getHead();

		next.x += head.x;
		next.y += head.y;

		return private.board.getBlock(next.x, next.y);
	};

	private.eat = function() {
		private.board.generateRandomTarget();
		private.settings.incrementScore();

		if (private.toolbar != undefined) {
			private.toolbar.update();
		}
	};

	private.render = function() {
		var next 	= private.getNext();
		var target 	= private.board.getTarget();
		var isEat 	= (target == next);
		var specs 	= private.settings.getData(); 

		if (isEat) {
			private.eat();
			private.explosions.boom(target.x * specs.size, target.y * specs.size);
		} else if (next == undefined || next.isActive) {
			$(window).trigger('hitError');
			return false;
		}

		private.snake.setFirst(next, isEat);
	};

	private.main();
	return public;

}
function VirtualKeyboard(direction) {

	var private = {};
	var public = {};

	private.direction = direction;

	var $html = $('<div class="keyboard"></div>');

	private.init = function() {
		private.createLeft();
		private.createRight();
		private.createUp();
		private.createDown();

		$html.children().on('click', function(){
			private.direction.mapKeycode($(this).data("keycode"));
		});
	};

	private.createLeft = function() {
		var $key = private.createKey();
		$key.addClass("fa-arrow-circle-o-left");
		$key.data('keycode', 37);
	};

	private.createUp = function() {
		var $key = private.createKey();
		$key.addClass("fa-arrow-circle-o-up");
		$key.data('keycode', 38);
	};

	private.createRight = function() {
		var $key = private.createKey();
		$key.addClass("fa-arrow-circle-o-right");
		$key.data('keycode', 39);
	};

	private.createDown = function() {
		var $key = private.createKey();
		$key.addClass("fa-arrow-circle-o-down");
		$key.data('keycode', 40);
	};

	private.createKey = function() {
		var $key = $('<i class="key fa"></i>');
		$html.append($key);
		return $key;
	};

	public.getHtml = function() {
		return $html;
	};

	private.init();
	return public;
	
};

function Keyboard(direction) {
	var public = {};
	var private = {};

	private.direction = direction;

	private.init = function() {
		$(document).keydown(function(event){
			private.direction.mapKeycode(event.keyCode);
		});
	};

	private.init();
	return public;
};

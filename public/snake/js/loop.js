function Loop() {
	var private = {};
	var public = {};

	private.fps 		= 10;
	private.interval 	= undefined;
	private.render 		= undefined;

	public.run = function() {
		private.interval = setInterval(private.render, 1000 / private.fps);
	};

	public.isInactive = function() {
		return private.interval == undefined;
	};

	public.stop = function() {
		clearInterval(private.interval);
	};

	public.on = function(event, callback) {
		private[event] = callback;
	};

	return public;
}
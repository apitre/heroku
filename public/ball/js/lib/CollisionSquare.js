define(["require", "exports"], function (require, exports) {
    "use strict";
    var CollisionSquare = (function () {
        function CollisionSquare() {
        }
        CollisionSquare.prototype.testVsSquare = function (a, b) {
            var dx = (a.x + a.width / 2) - (b.x + b.width / 2);
            var dy = (a.y + a.height / 2) - (b.y + b.height / 2);
            var width = (a.width + b.width) / 2;
            var height = (a.height + b.height) / 2;
            var crossWidth = width * dy;
            var crossHeight = height * dx;
            if (Math.abs(dx) <= width && Math.abs(dy) <= height) {
                if (crossWidth > crossHeight) {
                    var impact = (crossWidth > -crossHeight) ? { 'x': 0, 'y': 1 } : { 'x': -1, 'y': 0 };
                }
                else {
                    var impact = (crossWidth > -crossHeight) ? { 'x': 1, 'y': 0 } : { 'x': 0, 'y': -1 };
                }
                return this.create(a, b, impact);
            }
            return false;
        };
        CollisionSquare.prototype.create = function (a, b, delta) {
            var offset = {
                x: a.x,
                y: a.y
            };
            if (delta.x == -1) {
                offset.x = b.x - a.width;
            }
            if (delta.x == 1) {
                offset.x = b.x + b.width;
            }
            if (delta.y == -1) {
                offset.y = b.y - a.height;
            }
            if (delta.y == 1) {
                offset.y = b.y + b.height;
            }
            return {
                'offset': offset,
                'delta': delta
            };
        };
        return CollisionSquare;
    }());
    return CollisionSquare;
});

define(["require", "exports"], function (require, exports) {
    "use strict";
    var CollisionCircle = (function () {
        function CollisionCircle() {
        }
        CollisionCircle.prototype.testVsSquare = function (a, b) {
            var nearestX = Math.max(b.x, Math.min(a.x, b.a));
            var nearestY = Math.max(b.y, Math.min(a.y, b.b));
            var x = a.x - nearestX;
            var y = a.y - nearestY;
            if ((x * x) + (y * y) < (a.radius * a.radius)) {
                return this.create(a, {
                    'x': nearestX,
                    'y': nearestY
                });
            }
            return false;
        };
        CollisionCircle.prototype.create = function (circle, impact) {
            var a = Math.abs(circle.x - impact.x);
            var b = Math.abs(circle.y - impact.y);
            var c = Math.sqrt(a * a + b * b);
            var offset = {
                'x': impact.x + ((circle.radius / (c / a)) * this.getDelta(circle.x - impact.x)),
                'y': impact.y + ((circle.radius / (c / b)) * this.getDelta(circle.y - impact.y)),
            };
            var delta = {
                'x': (offset.x - impact.x) / circle.radius,
                'y': (offset.y - impact.y) / circle.radius
            };
            return {
                'offset': offset,
                'delta': delta
            };
        };
        CollisionCircle.prototype.getDelta = function (x) {
            return typeof x === 'number' ? x ? x < 0 ? -1 : 1 : x === x ? 0 : NaN : NaN;
        };
        return CollisionCircle;
    }());
    return CollisionCircle;
});

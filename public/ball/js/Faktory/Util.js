define(["require", "exports", "Controller/Home", "Model/Shape/Square", "Model/Canvas", "Model/Loop"], function (require, exports, Home, Square, Canvas, Loop) {
    "use strict";
    var Util = (function () {
        function Util() {
        }
        Util.buildHome = function ($, canvas) {
            return new Home($, canvas);
        };
        Util.builCanvas = function ($) {
            var square = new Square(0, 0, $(window).width(), $(window).height());
            return new Canvas($, square);
        };
        Util.buildLoop = function () {
            return new Loop();
        };
        return Util;
    }());
    return Util;
});

define(["require", "exports", "Faktory/Entities", "Faktory/Util"], function (require, exports, Entities, Util) {
    "use strict";
    return {
        'Entities': Entities,
        'Util': Util
    };
});

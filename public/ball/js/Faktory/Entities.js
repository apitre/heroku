define(["require", "exports", "Model/Shape/Square", "Model/Shape/Circle", "Controller/Entity/Human", "Controller/Entity/Block", "Controller/Entity/Ball", "Model/Physic", "Lib/CollisionSquare", "Lib/CollisionCircle"], function (require, exports, Square, Circle, Human, Block, Ball, Physic, CollisionSquare, CollisionCircle) {
    "use strict";
    var Entities = (function () {
        function Entities() {
        }
        Entities.buildHuman = function () {
            var shape = new Square(100, 100, 80, 100);
            var physic = new Physic();
            var collision = new CollisionSquare();
            return new Human(shape, physic, collision);
        };
        Entities.buildBall = function () {
            var shape = new Circle(100, 100, 20);
            var physic = new Physic();
            var collision = new CollisionCircle();
            return new Ball(shape, physic, collision);
        };
        Entities.buildBlock = function (x, y, width, height, ctx) {
            var shape = new Square(x, y, width, height);
            var block = new Block(shape);
            block.draw(ctx);
            return block;
        };
        Entities.buildBlockList = function (ctx) {
            return [
                this.buildBlock(200, 400, 320, 64, ctx),
                this.buildBlock(500, 200, 64, 64, ctx),
                this.buildBlock(20, 100, 64, 64, ctx),
                this.buildBlock(300, 100, 64, 64, ctx)
            ];
        };
        return Entities;
    }());
    return Entities;
});

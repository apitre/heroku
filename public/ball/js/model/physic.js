define(["require", "exports"], function (require, exports) {
    "use strict";
    var Physic = (function () {
        function Physic() {
            this.maxSpeed = 8;
            this.restitution = 0.95;
            this.mass = 1;
            this.time = 1;
            this.gravity = 0.5;
            this.friction = 0.98;
            this.speed = {
                x: 0,
                y: 0
            };
            this.applyForce = function (fx, fy) {
                this.speed.x += (fx / this.mass) * this.time;
                this.speed.y += (fy / this.mass) * this.time;
            };
        }
        Physic.prototype.apply = function (speed) {
            this.speed = speed;
            this.applyGravity();
            this.speed.x *= this.time;
            this.speed.y *= this.time;
            this.speed.x *= this.friction;
            this.speed.y *= this.friction;
        };
        ;
        Physic.prototype.applyCollision = function (dx, dy) {
            var speed = this.speed.x * dx + this.speed.y * dy;
            var force = -(this.restitution + 1) * speed * this.mass / this.time;
            var fx = (this.getDelta(this.speed.x) != dx) ? force * dx : 0;
            var fy = (this.getDelta(this.speed.y) != dy) ? force * dy : 0;
            this.applyForce(fx, fy);
        };
        ;
        Physic.prototype.applyGravity = function () {
            this.applyForce(0, this.mass * this.gravity);
        };
        ;
        Physic.prototype.getDelta = function (x) {
            return typeof x === 'number' ? x ? x < 0 ? -1 : 1 : x === x ? 0 : NaN : NaN;
        };
        return Physic;
    }());
    return Physic;
});

define(["require", "exports"], function (require, exports) {
    "use strict";
    var App = (function () {
        function App($, Faktory) {
            var canvas = Faktory.Util.builCanvas($);
            var controller = this.buildHomeController($, canvas, Faktory);
            var loop = Faktory.Util.buildLoop();
            $(window).resize(function () {
                canvas.updateSize($(this).width(), $(this).height());
                controller.updateSize();
            });
            $('body').append(canvas.getHtml());
            loop.onRender(function () {
                controller.render();
            });
            loop.run();
        }
        App.prototype.buildHomeController = function ($, canvas, Faktory) {
            var controller = Faktory.Util.buildHome($, canvas);
            var index = canvas.createLayer();
            var ctx = canvas.getCtx(index);
            controller.entity = Faktory.Entities.buildBall();
            controller.blocks = Faktory.Entities.buildBlockList(ctx);
            return controller;
        };
        return App;
    }());
    return App;
});

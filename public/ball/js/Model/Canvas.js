define(["require", "exports"], function (require, exports) {
    "use strict";
    var Canvas = (function () {
        function Canvas($, square) {
            this.nbLayers = 0;
            this.canvas = [];
            this.square = square;
            this.$layers = $('<div id="layers">');
        }
        Canvas.prototype.updateSize = function (width, height) {
            this.square.setDimension(width, height);
            for (var i = 1; i <= this.nbLayers; i++) {
                this.canvas[i].width = this.square.width;
                this.canvas[i].height = this.square.height;
            }
            ;
        };
        Canvas.prototype.getShape = function () {
            return this.square;
        };
        Canvas.prototype.createLayer = function () {
            var canvas = document.createElement('canvas');
            this.nbLayers++;
            canvas.id = "layer-" + this.nbLayers;
            canvas.width = this.square.width;
            canvas.height = this.square.height;
            canvas.style.zIndex = String(this.nbLayers);
            this.canvas[this.nbLayers] = canvas;
            this.$layers.append(canvas);
            return this.nbLayers;
        };
        Canvas.prototype.getCanvas = function (index) {
            return this.canvas[index];
        };
        ;
        Canvas.prototype.getHtml = function () {
            return this.$layers;
        };
        ;
        Canvas.prototype.getCtx = function (index) {
            return this.canvas[index].getContext('2d');
        };
        ;
        return Canvas;
    }());
    return Canvas;
});

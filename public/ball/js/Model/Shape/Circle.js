define(["require", "exports"], function (require, exports) {
    "use strict";
    var Circle = (function () {
        function Circle(x, y, radius) {
            this.x = x;
            this.y = y;
            this.radius = radius;
        }
        return Circle;
    }());
    return Circle;
});

define(["require", "exports"], function (require, exports) {
    "use strict";
    var Square = (function () {
        function Square(x, y, width, height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.a = this.x + this.width;
            this.b = this.y + this.height;
        }
        Square.prototype.getCenterX = function () {
            return this.x + (this.width / 2);
        };
        Square.prototype.getCenterY = function () {
            return this.y + (this.height / 2);
        };
        Square.prototype.setDimension = function (width, height) {
            this.width = width;
            this.height = height;
            this.a = this.x + this.width;
            this.b = this.y + this.height;
        };
        return Square;
    }());
    return Square;
});

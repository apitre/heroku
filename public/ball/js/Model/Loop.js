define(["require", "exports"], function (require, exports) {
    "use strict";
    var Loop = (function () {
        function Loop() {
            this.fps = 10;
        }
        Loop.prototype.run = function () {
            this.interval = setInterval(this.render, this.fps);
        };
        Loop.prototype.isInactive = function () {
            return this.interval == undefined;
        };
        Loop.prototype.stop = function () {
            clearInterval(this.interval);
        };
        Loop.prototype.onRender = function (callback) {
            this.render = callback;
        };
        return Loop;
    }());
    return Loop;
});

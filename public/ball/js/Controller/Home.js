define(["require", "exports"], function (require, exports) {
    "use strict";
    var Home = (function () {
        function Home($, canvas) {
            var _this = this;
            this.keys = {};
            this.speed = {
                x: 0,
                y: 0
            };
            this.canvas = canvas;
            this.board = this.canvas.getShape();
            var index = this.canvas.createLayer();
            this.ctx = this.canvas.getCtx(index);
            $(document).keydown(function (event) {
                _this.keys[event.keyCode] = true;
            });
            $(document).keyup(function (event) {
                _this.keys[event.keyCode] = false;
            });
        }
        Home.prototype.handleKeys = function () {
            var maxSpeed = this.entity.getMaxSpeed();
            if (this.keys[37] && this.speed.x > -maxSpeed) {
                this.speed.x--;
            }
            if (this.keys[39] && this.speed.x < maxSpeed) {
                this.speed.x++;
            }
            if (this.keys[38] && this.speed.y > -maxSpeed) {
                this.speed.y--;
            }
            if (this.keys[40] && this.speed.y < maxSpeed) {
                this.speed.y++;
            }
        };
        Home.prototype.updateSize = function () {
            for (var i = 0; i < this.blocks.length; i++) {
                this.blocks[i].draw(this.canvas.getCtx(1));
            }
        };
        Home.prototype.render = function () {
            this.handleKeys();
            this.entity.updatePhysic(this.speed);
            this.entity.checkBounds(this.board);
            this.entity.checkCollisions(this.blocks);
            this.ctx.clearRect(0, 0, this.board.width, this.board.height);
            this.entity.draw(this.ctx);
        };
        return Home;
    }());
    return Home;
});

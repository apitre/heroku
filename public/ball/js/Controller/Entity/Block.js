define(["require", "exports"], function (require, exports) {
    "use strict";
    var Block = (function () {
        function Block(shape) {
            this.color = "#000000";
            this.shape = shape;
        }
        Block.prototype.draw = function (ctx) {
            ctx.beginPath();
            ctx.rect(this.shape.x, this.shape.y, this.shape.width, this.shape.height);
            ctx.fillStyle = this.color;
            ctx.fill();
        };
        return Block;
    }());
    return Block;
});

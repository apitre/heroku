var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "Controller/Entity/AbstractEntity"], function (require, exports, Entity) {
    "use strict";
    var Human = (function (_super) {
        __extends(Human, _super);
        function Human(shape, physic, collision) {
            _super.call(this, shape, physic, collision);
            this.color = "red";
            this.physic.restitution = 0;
        }
        Human.prototype.draw = function (ctx) {
            ctx.beginPath();
            ctx.rect(this.shape.x, this.shape.y, this.shape.width, this.shape.height);
            ctx.fillStyle = this.color;
            ctx.fill();
        };
        Human.prototype.checkBounds = function (square) {
            if (this.shape.x < square.x) {
                this.shape.x = square.x;
                this.physic.applyCollision(1, 0);
            }
            if (this.shape.x > square.width - this.shape.width) {
                this.shape.x = square.width - this.shape.width;
                this.physic.applyCollision(-1, 0);
            }
            if (this.shape.y < square.y) {
                this.shape.y = square.y;
                this.physic.applyCollision(0, 1);
            }
            if (this.shape.y > square.height - this.shape.height) {
                this.shape.y = square.height - this.shape.height;
                this.physic.applyCollision(0, -1);
            }
        };
        return Human;
    }(Entity));
    return Human;
});

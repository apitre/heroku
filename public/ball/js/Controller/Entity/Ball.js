var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "Controller/Entity/AbstractEntity"], function (require, exports, Entity) {
    "use strict";
    var Ball = (function (_super) {
        __extends(Ball, _super);
        function Ball(shape, physic, collision) {
            _super.call(this, shape, physic, collision);
            this.color = "red";
        }
        Ball.prototype.draw = function (ctx) {
            ctx.beginPath();
            ctx.arc(this.shape.x, this.shape.y, this.shape.radius, 0, Math.PI * 2);
            ctx.fillStyle = this.color;
            ctx.fill();
        };
        Ball.prototype.checkBounds = function (square) {
            if (this.shape.x < this.shape.radius) {
                this.shape.x = this.shape.radius;
                this.physic.applyCollision(1, 0);
            }
            if (this.shape.x > square.width - this.shape.radius) {
                this.shape.x = square.width - this.shape.radius;
                this.physic.applyCollision(-1, 0);
            }
            if (this.shape.y < this.shape.radius) {
                this.shape.y = this.shape.radius;
                this.physic.applyCollision(0, 1);
            }
            if (this.shape.y > square.height - this.shape.radius) {
                this.shape.y = square.height - this.shape.radius;
                this.physic.applyCollision(0, -1);
            }
        };
        return Ball;
    }(Entity));
    return Ball;
});

define(["require", "exports"], function (require, exports) {
    "use strict";
    var Entity = (function () {
        function Entity(shape, physic, collision) {
            this.shape = shape;
            this.physic = physic;
            this.collision = collision;
        }
        Entity.prototype.checkCollisions = function (blocks) {
            for (var i = 0; i < blocks.length; i++) {
                var collision = this.collision.testVsSquare(this.shape, blocks[i].shape);
                //console.log(collision);
                if (collision) {
                    this.shape.x = collision.offset.x;
                    this.shape.y = collision.offset.y;
                    this.physic.applyCollision(collision.delta.x, collision.delta.y);
                }
            }
        };
        Entity.prototype.updatePhysic = function (speed) {
            this.physic.apply(speed);
            this.shape.y += this.physic.speed.y;
            this.shape.x += this.physic.speed.x;
        };
        Entity.prototype.getMaxSpeed = function () {
            return this.physic.maxSpeed;
        };
        return Entity;
    }());
    return Entity;
});

define(["require", "exports"], function (require, exports) {
    "use strict";
    var Entity = (function () {
        function Entity(shape, physic, collision) {
            this.shape = shape;
            this.physic = physic;
            this.collision = collision;
        }
        return Entity;
    }());
    return Entity;
});

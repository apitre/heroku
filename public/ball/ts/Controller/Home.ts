class Home {

    canvas;
	entity;
	water;
	blocks;

	board;
	ctx;
	keys: { [key: number]: boolean } = {};

	speed = {
		x : 0,
		y : 0
	}

    constructor($, canvas) {
        this.canvas = canvas;

        this.board = this.canvas.getShape();

        var index = this.canvas.createLayer();

		this.ctx = this.canvas.getCtx(index);

		$(document).keydown((event) => {
            this.keys[event.keyCode] = true;
        });
			
		$(document).keyup((event) => {
            this.keys[event.keyCode] = false;
        });
    }

    handleKeys() : void {
        var maxSpeed = this.entity.getMaxSpeed();

		if (this.keys[37] && this.speed.x > -maxSpeed) {
        	this.speed.x--;
        }

		if (this.keys[39] && this.speed.x < maxSpeed) {
            this.speed.x++;
        }

        if (this.keys[38] && this.speed.y > -maxSpeed) {
        	this.speed.y--;
        }

		if (this.keys[40] && this.speed.y < maxSpeed) {
        	this.speed.y++;
        }
    }

    updateSize() : void {
        for (var i = 0; i < this.blocks.length; i++) {
			this.blocks[i].draw(this.canvas.getCtx(1));
		}
    }

    render() : void {
        this.handleKeys();

		this.entity.updatePhysic(this.speed);

		this.entity.checkBounds(this.board);

		this.entity.checkCollisions(this.blocks);

    	this.ctx.clearRect(0, 0, this.board.width, this.board.height);

    	this.entity.draw(this.ctx);
    }

}

export = Home;
import Entity = require("Controller/Entity/AbstractEntity");

class Human extends Entity {

	color = "red";

	constructor(shape, physic, collision) {
		super(shape, physic, collision);
		this.physic.restitution = 0;
	}

	draw(ctx) : void {
		ctx.beginPath();
		ctx.rect(this.shape.x, this.shape.y, this.shape.width, this.shape.height);
		ctx.fillStyle = this.color;
		ctx.fill();
	}

	checkBounds(square) : void {
		if (this.shape.x < square.x) {
			this.shape.x = square.x;
			this.physic.applyCollision(1, 0);
		}

		if (this.shape.x > square.width - this.shape.width) {
			this.shape.x = square.width - this.shape.width;
			this.physic.applyCollision(-1, 0);
		}

		if (this.shape.y < square.y) {
			this.shape.y = square.y;
			this.physic.applyCollision(0, 1);
		}

		if (this.shape.y > square.height - this.shape.height) {
			this.shape.y = square.height - this.shape.height;
			this.physic.applyCollision(0, -1);
		}
	} 

} 

export = Human;
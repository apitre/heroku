class Block {

	shape;
	color = "#000000";

	constructor(shape) {
		this.shape = shape;
	}

    draw(ctx) : void {
        ctx.beginPath();
		ctx.rect(this.shape.x, this.shape.y, this.shape.width, this.shape.height);
		ctx.fillStyle = this.color;
		ctx.fill();
    }

} 

export = Block;
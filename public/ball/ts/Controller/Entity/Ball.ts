import Entity = require("Controller/Entity/AbstractEntity");

class Ball extends Entity {

	color = "red";

	constructor(shape, physic, collision) {
		super(shape, physic, collision);
	}

	draw(ctx) : void {
		ctx.beginPath();
	    ctx.arc(this.shape.x, this.shape.y, this.shape.radius, 0, Math.PI * 2);
	    ctx.fillStyle = this.color;
	    ctx.fill();
	}

	checkBounds(square) : void {
		if (this.shape.x < this.shape.radius) {
			this.shape.x = this.shape.radius;
			this.physic.applyCollision(1, 0);
		}

		if (this.shape.x > square.width - this.shape.radius) {
			this.shape.x = square.width - this.shape.radius;
			this.physic.applyCollision(-1, 0);
		}

		if (this.shape.y < this.shape.radius) {
			this.shape.y = this.shape.radius;
			this.physic.applyCollision(0, 1);
		}

		if (this.shape.y > square.height - this.shape.radius) {
			this.shape.y = square.height - this.shape.radius;
			this.physic.applyCollision(0, -1);
		}
	} 

} 

export = Ball;
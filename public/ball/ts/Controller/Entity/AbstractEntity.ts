abstract class Entity {
	shape;
	physic;
	collision;

	constructor(shape, physic, collision) {
		this.shape = shape;
		this.physic = physic;
		this.collision = collision;
    }

	abstract draw(ctx) : void;

	abstract checkBounds(square) : void;

	checkCollisions(blocks) : void {
		for (var i = 0; i < blocks.length; i++) {
			var collision = this.collision.testVsSquare(this.shape, blocks[i].shape);

			//console.log(collision);

			if (collision) {
				this.shape.x = collision.offset.x;
				this.shape.y = collision.offset.y;

				this.physic.applyCollision(collision.delta.x, collision.delta.y);
			}
		}
	}

	updatePhysic(speed) : void{
		this.physic.apply(speed);
	    this.shape.y += this.physic.speed.y;
	    this.shape.x += this.physic.speed.x;
	}

	getMaxSpeed() : number {
		return this.physic.maxSpeed;
	}
}

export = Entity;
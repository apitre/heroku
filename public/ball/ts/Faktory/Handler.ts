import Entities = require("Faktory/Entities");
import Util = require("Faktory/Util");

export  = {
    'Entities'  : Entities,
    'Util'      : Util
}
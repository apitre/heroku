import Home = require("Controller/Home");
import Square = require("Model/Shape/Square");
import Canvas = require("Model/Canvas");
import Loop = require("Model/Loop");

class Util {

    static buildHome($, canvas) : Home {
        return new Home($, canvas);
    }

    static builCanvas($)  : Canvas {
		var square = new Square(0, 0, $(window).width(), $(window).height());
		return new Canvas($, square);
	}

    static buildLoop() : Loop {
        return new Loop();
    }

}

export = Util;
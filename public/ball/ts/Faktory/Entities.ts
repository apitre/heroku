import Square = require("Model/Shape/Square");
import Circle = require("Model/Shape/Circle");
import Human = require("Controller/Entity/Human");
import Block = require("Controller/Entity/Block");
import Ball = require("Controller/Entity/Ball");
import Physic = require("Model/Physic");
import CollisionSquare = require("Lib/CollisionSquare");
import CollisionCircle = require("Lib/CollisionCircle");

class Entities {

    static buildHuman() : Human {
        var shape = new Square(100, 100, 80, 100);
        var physic = new Physic();
        var collision = new CollisionSquare();
        return new Human(shape, physic, collision);
    }

    static buildBall() : Ball {
        var shape = new Circle(100, 100, 20);
        var physic = new Physic();
        var collision = new CollisionCircle();
        return new Ball(shape, physic, collision);
    }

    static buildBlock(x, y, width, height, ctx) : Block {
        var shape = new Square(x, y, width, height);
        var block = new Block(shape);
        block.draw(ctx);
        return block;
    }

    static buildBlockList(ctx) : [Block] {
        return [
			this.buildBlock(200, 400, 320, 64, ctx),
			this.buildBlock(500, 200, 64, 64, ctx),
			this.buildBlock(20, 100, 64, 64, ctx),
            this.buildBlock(300, 100, 64, 64, ctx)
		];
    }

}

export = Entities;
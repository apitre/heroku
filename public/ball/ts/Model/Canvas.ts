class Canvas {

    nbLayers = 0;
	square;
    canvas = [];
    $layers;

    constructor($, square) {
        this.square = square;
        this.$layers = $('<div id="layers">');
    }

    updateSize(width, height) : void {
        this.square.setDimension(width, height);

		for (var i = 1; i <= this.nbLayers; i++) {
			this.canvas[i].width = this.square.width;
			this.canvas[i].height = this.square.height;
		};
    }

    getShape() {
        return this.square;
    }

    createLayer() {
        var canvas = document.createElement('canvas');

		this.nbLayers++;

		canvas.id     			= "layer-"+this.nbLayers;
		canvas.width  			= this.square.width;
		canvas.height 			= this.square.height;
		canvas.style.zIndex   	= String(this.nbLayers);

		this.canvas[this.nbLayers] = canvas;

		this.$layers.append(canvas);

		return this.nbLayers;
    }

   getCanvas(index: number) {
		return this.canvas[index];
	};

	getHtml() {
		return this.$layers;
	};

	getCtx(index: number) {
		return this.canvas[index].getContext('2d');
	};

}

export = Canvas;
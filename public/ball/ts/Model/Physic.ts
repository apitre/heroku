class Physic {
	maxSpeed = 8;
	restitution = 0.95;
	mass = 1;
	time = 1;
	gravity = 0.5;
	friction = 0.98;
	speed = {
		x : 0,
		y : 0
	};
	
	apply(speed) {
		this.speed = speed;

		this.applyGravity();

		this.speed.x *= this.time;
		this.speed.y *= this.time;

		this.speed.x *= this.friction;
		this.speed.y *= this.friction;
	};

	applyCollision(dx, dy) {
		var speed = this.speed.x * dx + this.speed.y * dy;

		var force = -(this.restitution + 1) * speed * this.mass / this.time;

		var fx = (this.getDelta(this.speed.x) != dx) ? force * dx : 0;

		var fy = (this.getDelta(this.speed.y) != dy) ? force * dy : 0;

		this.applyForce(fx, fy);
	};

	private applyGravity() {
		this.applyForce(0, this.mass * this.gravity);
	};

	private applyForce = function(fx, fy) {
		this.speed.x += (fx / this.mass) * this.time;
		this.speed.y += (fy / this.mass) * this.time;
	};

	private getDelta(x): any {
    	return typeof x === 'number' ? x ? x < 0 ? -1 : 1 : x === x ? 0 : NaN : NaN;
	}

}

export = Physic;
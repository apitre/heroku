class Loop {
	fps = 10;
	interval;
	render;

	run() {
		this.interval = setInterval(this.render, this.fps);
	}

	isInactive() {
		return this.interval == undefined;
	}

	stop() {
		clearInterval(this.interval);
	}

	onRender(callback) {
        this.render = callback;
    }
}

export = Loop;
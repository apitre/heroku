class Circle {

    x: number;
    y: number;
    radius: number;
    
    constructor(x, y, radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }
}

export = Circle;
class Square {

    x: number;
    y: number;
    height: number;
    width: number;
    a: number;
    b: number;

    constructor(x, y, width, height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.a = this.x + this.width;
        this.b = this.y + this.height;
    }

    getCenterX(): number {
        return this.x + (this.width / 2);
    }

    getCenterY(): number {
        return this.y + (this.height / 2);
    }

    setDimension(width: number, height:number): void {
        this.width = width;
        this.height = height;
        this.a = this.x + this.width;
        this.b = this.y + this.height;
    }
    
}

export = Square;
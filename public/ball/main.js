requirejs.config({
    baseUrl: '/ball/js',
    paths: {
		jquery: 'lib/jquery'
	}
});

require(['jquery', 'App', 'Faktory/Handler',], function($, App, Faktory){
	$("document").ready(function(){
		var app = new App($, Faktory);
	});
});
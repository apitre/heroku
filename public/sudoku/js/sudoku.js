define({

    isValid : true,

    bind : function(grille, html){
        var $html = $("<div>"+html+"</div>");

        $html.find('.b').on('change', function(){
            var x = $(this).data('col');
            var y = $(this).data('row');
            grille[y][x] = parseInt($(this).val());
            if ($(this).val() == 0) {
                $(this).val("")
            } 
        });

        return $html;
    },

    time : function(fn, message){
        var start = Date.now();
        fn();
        var end = Date.now();

        if (message == undefined) {
            message = "Solved in: "
        }

        console.info(message + (end - start) / 1000 + " secs");
    },

    print : function(grille, clear){
        var html = "";

        for (var i=0; i<9; i++) {
            for (var j=0; j<9; j++) {
                var value = grille[i][j];

                if (value == 0) {
                    value = "";
                }

                html += "<input class='b c"+j+" r"+i+"' type='text' value='"+value+"' data-row='"+i+"' data-col='"+j+"'>";
            }
            html += "<br>";
        }

        var $html = this.bind(grille, html);

        $('#grille').html($html);
    },

    reset : function(){
        this.isValid = true;
    },

    solveEasy : function(grille){
        var hasChange = false;

        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (grille[y][x] == 0) {
                    var options = this.getOptions(grille, y, x);
                    if (options.length == 1) {
                        grille[y][x] = options[0];
                        hasChange = true;
                    }
                }

            }
        }

        if (hasChange && this.isValid){
            this.solveEasy(grille);
        }
    },

    getOptions : function(grille, y, x){

        var takens = {
            1 : 0,
            2 : 0,
            3 : 0,
            4 : 0,
            5 : 0,
            6 : 0,
            7 : 0,
            8 : 0,
            9 : 0
        };

        for (var i = 0; i < 9; i++) {
            if (grille[y][i] > 0) {
                takens[grille[y][i]]++;
            }
            if (grille[i][x] > 0) {
                takens[grille[i][x]]++;
            }
        }

        var a = y - (y % 3);
        var b = x - (x % 3);

        for (var i=a; i < a+3; i++) {
            for (var j=b; j < b+3; j++) {
                if (grille[i][j] > 0) {
                    takens[grille[i][j]]++;
                }
            }
        }

        var options = [];

        for (var i = 1; i <= 9; i++) {
            if (takens[i] == 0) {
                options.push(i);
            }
            if (takens[i] > 3) {
                this.isValid = false;
            }
        }

        return options;
    },

    inArray : function(needle, haystack)
    {
        return haystack.indexOf(needle) != -1;
    },

    isFree : function(k, grille, y, x){
        for (var i = 0; i < 9; i++) {
            if (grille[y][i] == k) {
                return false;
            }
            if (grille[i][x] == k) {
                return false;
            }
        }

        var a = y - (y % 3);
        var b = x - (x % 3);

        for (var i=a; i < a + 3; i++) {
            for (var j=b; j < b + 3; j++) {
                if (grille[i][j] == k) {
                    return false;
                }
            }
        }

        return true;
    },

    preparePost : function(grille){
        var grilleStr = [];

        for(var i = 0; i < grille.length; i++) {
            grilleStr.push("["+grille[i].join()+"]");
        }

        return {
            'grille' : "["+grilleStr.join()+"]"
        }
    },

    solve : function(grille, position){
        if (position == 9 * 9 || !this.isValid) {
            return true;
        }

        var i = Math.floor(position / 9);
        var j = position % 9;

        if (grille[i][j] != 0) {
            return this.solve(grille, position + 1);
        }

        for (var k = 1; k <= 9; k++) {
            if (this.isFree(k, grille, i, j)) {
                grille[i][j] = k;
                if (this.solve(grille, position + 1)) {
                    return true;
                }
            }
        }

        grille[i][j] = 0;
        return false;
    }

});
